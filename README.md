LETGO TEST TWITTER LATEST TWEETS
============

# Requirements:

We need a small REST API with one endpoint that given a Twitter username, should return the user’s last 10 tweets, in JSON format.

Feel free to use any frameworks or libraries you need. 
If you want to use a framework, take into account that we use Symfony2.
The application should not save anything to MySQL or to any other relational DB.

We will value:

 Code tested with Behat and/or PHPUnit (or with any other testing tool).
 The code's architecture and the paradigm usage like Hexagonal Architecture and DDD.

Bonus:

  Improve the application’s speed caching the response using Memcache(d), Redis or any other system you may know.

# Brief description on implementation

* About TWITTER API: The implementation is based on the traditional REST API of Twitter. However the whole implementation could have been
done by using :

Streaming APIs
https://dev.twitter.com/streaming/overview

Finally I decided for the sake of simplicity and time, and because I guess this is not the goal of this exercise, I 
went to the traditional and striaghtforward API. Also because you mentioned that you don't want to store in any 
persistance layer (Database-Mysql). So the API used is:
 
 https://dev.twitter.com/rest/reference/get/statuses/user_timeline
 
* About Architecture: I am not an expert in Symfony2, I was working from 6 to 9 months, however  in the last year
 I moved to Yii2. So the fact to implement DDD within Symfony2 it is been a challenge for me.Finally you will see
 that I implement a pseudo DDD, but from my point of view it acomplishes the AGNOSTIC framework and decouple goals.

* The url that I was using is:

 http://api.letgolocalhost.com/

 http://api.letgolocalhost.com/api/v1/twitter/last-tweets/lonelyplanet/10
 http://api.letgolocalhost.com/api/v1/twitter/info

 Where the amount of tweets is optional. It works.

* About Memcached: I didn't implement anything fancy here. Speciallydue to a lack of time.
We could start talking about cache dependency, etc.

* About my problems: Well, I have to admit I had many issues implementing the client for Twitter.
The reason is the motherXXXX that created GUZZLE and keep changing versions every so and on.
I guess he is bored with his life. So this made me waste a loooooooot of time, and that is why there are some parts
like tests not perfect.
I have to admit too that the UNIT tests and functional tests are not my strong point.


# Requirements

* Symfony2 latest version
* apt-get install phpunit
* apt-get install php5-dev php-pear
* apt-get install php5-curl
* apt-get install memcached
* sudo apt-get install php5-memcached php5-memcache