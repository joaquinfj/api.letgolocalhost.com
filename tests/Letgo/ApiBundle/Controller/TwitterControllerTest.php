<?php

namespace Tests\Letgo\ApiBundle\Controller;

use Tests\Letgo\ApiBundle\ApiWebTestCase;

/**
 * Class TwitterControllerTest
 * @covers TwitterController
 * @package Tests\ApiBundle\Controller
 */
class TwitterControllerTest extends ApiWebTestCase
{
    /**
     * Asserts URLS and routing
     * @return bool
     */
    public function testUrls()
    {
        $urlWs = self::$router->generate('api_twitter_info', array(), false);
        $this->assertEquals('/api/v1/twitter/info', $urlWs, 'The urls generation for API tests fails. Again Symfony2');

        $urlWs = self::$router->generate(
            'api_twitter_lasttweets',
            ['user'=>'joaquinfj', 'amount'=>15],
            false
        );
        $this->assertEquals('/api/v1/twitter/last-tweets/joaquinfj/15', $urlWs, 'The urls for last tweets '.
            'generation for API tests fails. Again Symfony2');
        
        return true;
    }

    /**
     * Tests basic home page and the text that appears
     * @test
     * @covers TwitterController::indexAction
     */
    public function testInfo()
    {
        $client = static::createApiClient();
        $client->request('GET', '/api/v1/twitter/info');
        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getContent(), true);

        $this->assertSame(array('version' => '1.0'), $data['info']);
        return true;
    }

    /**
     * Tests basic home page and the text that appears
     * @test
     * @covers TwitterController::indexAction
     */
    public function testLastTweets()
    {
        $client = static::createApiClient();

        $username = 'lonelyplanet';
        $amount = 10;
        $urlWs = self::$router->generate('api_twitter_lasttweets', ['user'=>$username, 'amount'=>$amount], false);

        $client->request('GET', $urlWs);
        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getContent(), true);

        $this->assertLessThanOrEqual($amount, sizeof($data['lastTweets']), 'No more than '.$amount.' tweets expected');

        return true;
    }
}
