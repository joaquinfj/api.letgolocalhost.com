<?php

namespace Tests\Letgo\ApiBundle;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Routing\Generator\UrlGenerator;

/**
 * Class ApiWebTestCase
 * Base class for all tests for API's
 * @package Tests\ApiBundle
 */
class ApiWebTestCase extends WebTestCase
{
    /* @var UrlGenerator $router */
    protected static $router;

    /**
     *
     */
    protected function setUp()
    {
        $client = parent::createClient();
        self::$router = $client->getContainer()->get('router');

        parent::setUp();
    }

    /**
     * @param array $options
     * @param array $server
     * @param bool $withDebug
     * @return \Symfony\Bundle\FrameworkBundle\Client
     */
    protected static function createApiClient(array $options = array(), array $server = array(), $withDebug = true)
    {
        $client = parent::createClient($options, $server);
        if ($withDebug) {
            $cookie = new Cookie('XDEBUG_SESSION', 'PHPSTORM');
            $client->getCookieJar()->set($cookie);
        }

        return $client;
    }
}
