<?php

namespace Tests\Letgo\Domain\Rules;

use Letgo\Domain\Entities\Tweet;
use Letgo\Domain\Entities\TwitterUser;
use Letgo\Domain\Rules\UserRu;
use Tests\Letgo\Domain\DomainTestCase;

/**
 * Class UserRuTest
 * @covers UserRu
 *
 * @package DomainTestCase
 */
class UserRuTest extends DomainTestCase
{
    /**
     * @test UserRu::getLastTweets
     * @return bool
     */
    public function testGetLastTweets()
    {
        $fakeTweet1 = new Tweet();
        $fakeTweet1->setId(666);
        $fakeTweet2 = new Tweet();
        $fakeTweet2->setId(777);
        $fakeTweets = [$fakeTweet1, $fakeTweet2];

        $repo = $this->getMockBuilder('Letgo\DomainInfraBundle\Repositories\TweetRepository')
            ->disableOriginalConstructor()
            ->getMock();
        $repo
            ->expects($this->once())
            ->method('findLastTweets')
            ->will($this->returnValue($fakeTweets));

        $twiterUser = new TwitterUser();
        $twiterUser->setUsername('joaquinfj');

        $userRu = new UserRu($repo);

        /** @var Tweet[] $tweets */
        $tweets = $userRu->getLastTweets($twiterUser, 10);
        $this->assertEquals(2, sizeof($tweets), 'This account only has 2 tweets. ');

        $this->assertEquals('Letgo\Domain\Entities\Tweet', get_class($tweets[1]));
        $this->assertEquals(666, $tweets[0]->getId());

        return true;
    }
}
