<?php

namespace Tests\AppBundle;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class AppWebTestCase
 * Base class for all tests for UI WEB,functional tests
 * @package Tests\AppBundle
 */
class AppWebTestCase extends WebTestCase
{
    /**
     * @param array $options
     * @param array $server
     * @return \Symfony\Bundle\FrameworkBundle\Client
     */
    protected static function createApiClient(array $options = array(), array $server = array())
    {
        return parent::createClient($options, $server);
    }
}