<?php

namespace Tests\AppBundle\Controller;

use Tests\AppBundle\AppWebTestCase;

/**
 * Class DefaultControllerTest
 * @covers DefaultController
 * @package Tests\AppBundle\Controller
 */
class DefaultControllerTest extends AppWebTestCase
{
    /**
     * Tests basic home page and the text that appears
     * @test
     * @covers DefaultController::indexAction
     */
    public function testIndex()
    {
        $client = static::createApiClient();
        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Welcome to LETGO', $crawler->filter('#container h1')->text());

        return true;
    }

    /**
     * Tests basic home page, asserts link first sentence
     * @test
     * @covers DefaultController::indexAction
     */
    public function testLinkAssertFirstPage()
    {
        $client = static::createApiClient();
        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains(
            'Issues implemented and requested by you, not a 3 hours test',
            $crawler->filter('#next')->text()
        );

        return true;
    }
}
