<?php

namespace Letgo\Domain\Repositories;

use Letgo\Domain\Entities\Tweet;

/**
 * Interface TweetRepositoryInterface
 */
interface TweetRepositoryInterface
{    
    /**
     * @param int $tweetId
     * @return Tweet|null
     */
    public function find($tweetId);

    /**
     * @param string $username
     * @param int $count
     * @return Tweet[]|null
     */
    public function findLastTweets($username, $count);

    /**
     * @param string $username
     * @param int $count
     * @return string|null
     */
    public function findLastTweetsJson($username, $count);

    /**
     * @param Tweet $tweet
     * @return int
     */
    public function postTweet(Tweet $tweet);
}