<?php

namespace Letgo\Domain\Services;

/**
 * Interface TwitterClientInterface
 * @package Letgo\Domain\Services
 */
interface TwitterClientInterface
{
    /**
     * @see https://dev.twitter.com/rest/reference/get/statuses/user_timeline
     * @param string $screenName
     * @param int $count
     * @return string
     */
    public function getUserTimeline($screenName, $count);

    /**
     * @see https://dev.twitter.com/rest/reference/get/statuses/show/%3Aid
     * @param int $tweetId
     * @return string
     */
    public function getTweetById($tweetId);
}