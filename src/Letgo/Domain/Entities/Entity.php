<?php

namespace Letgo\Domain\Entities;

/**
 * Class Entity
 * @package Letgo
 */
abstract class Entity
{

    /**
     * Entity constructor.
     */
    public function __construct()
    {
    }
}