<?php

namespace Letgo\Domain\Entities;

/**
 * Class TwitterUser
 * @package Letgo\Domain\Entities
 */
class TwitterUser extends Entity
{
    /** @var  int $twitterUserId */
    private $twitterUserId;
    /** @var  string $username */
    private $username;
    /** @var  string $jsonResponse */
    private $jsonResponse;

    /**
     * TwitterUser constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return int
     */
    public function getTwitterUserId()
    {
        return $this->twitterUserId;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getJsonResponse()
    {
        return $this->jsonResponse;
    }

    /**
     * @param int $twitterUserId
     */
    public function setTwitterUserId($twitterUserId)
    {
        $this->twitterUserId = $twitterUserId;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @param string $jsonResponse
     */
    public function setJsonResponse($jsonResponse)
    {
        $this->jsonResponse = $jsonResponse;
    }
}