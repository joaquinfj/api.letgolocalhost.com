<?php

namespace Letgo\Domain\Entities;

/**
 * Class Tweet
 * @package Letgo\Domain\Entities
 */
class Tweet extends Entity
{
    /** @var  int $id */
    private $id;
    /** @var  int $twitterUserId */
    private $twitterUserId;
    /** @var string $jsonResponse */
    private $jsonResponse;

    /**
     * Tweet constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getTwitterUserId()
    {
        return $this->twitterUserId;
    }

    /**
     * @return string
     */
    public function getJsonResponse()
    {
        return $this->jsonResponse;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param int $twitterUserId
     */
    public function setTwitterUserId($twitterUserId)
    {
        $this->twitterUserId = $twitterUserId;
    }

    /**
     * @param string $jsonResponse
     */
    public function setJsonResponse($jsonResponse)
    {
        $this->jsonResponse = $jsonResponse;
    }
}