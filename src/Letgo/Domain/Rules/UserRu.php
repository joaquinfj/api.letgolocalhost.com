<?php

namespace Letgo\Domain\Rules;

use Letgo\Domain\Entities\Tweet;
use Letgo\Domain\Entities\TwitterUser;
use Letgo\Domain\Repositories\TweetRepositoryInterface;

/**
 * Class UserRu
 * @package Letgo\Domain\Rules
 */
class UserRu extends Rule
{
    /** @var  TweetRepositoryInterface $tweetRepository */
    private $tweetRepository;

    /**
     * UserRu constructor.
     * @param TweetRepositoryInterface $tweetRepo
     */
    public function __construct(TweetRepositoryInterface $tweetRepo)
    {
        $this->tweetRepository = $tweetRepo;
    }

    /*
     * @param TwitterUser $user
     * @param int $count
     * @return Tweet[]|null
     */
    public function getLastTweets(TwitterUser $user, $count = 10, $format = 'php')
    {
        if ($format=='json') {
            $tweets = $this->tweetRepository->findLastTweetsJson($user->getUsername(), $count);
        } else {
            $tweets = $this->tweetRepository->findLastTweets($user->getUsername(), $count);
        }

        return $tweets;
    }
}