<?php

namespace Letgo\ApiBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class LetgoApiBundle
 * @package Letgo\ApiBundle
 */
class LetgoApiBundle extends Bundle
{
}
