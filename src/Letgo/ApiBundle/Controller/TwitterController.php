<?php

namespace Letgo\ApiBundle\Controller;

use GuzzleHttp\Client;
use GuzzleHttp\Tests\Oauth1;

use Letgo\Domain\Entities\TwitterUser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class TwitterController
 * @package Letgo\ApiBundle\Controller
 */
class TwitterController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function infoAction()
    {
        $data = ['info'=>['version' => '1.0']];

        // calls json_encode and sets the Content-Type header
        return new JsonResponse($data);
    }

    /**
     * @param string $user
     * @param int|null $amount
     * @return JsonResponse
     */
    public function lastTweetsAction($user, $amount = null)
    {
        /** @var TwitterUser $twitterUser */
        $twitterUser = $this->get('letgo.domain.entities.twitter_user');
        $twitterUser->setUsername($user);

        $userRu = $this->get('letgo.domain.rules.user_ru');
        $data = ['user'=>$twitterUser->getUsername(), 'lastTweets'=>[]];
        $data['lastTweets'] = $userRu->getLastTweets($twitterUser, $amount, 'json');

        // Calls json_encode and sets the Content-Type header
        return new JsonResponse($data);
    }
}
