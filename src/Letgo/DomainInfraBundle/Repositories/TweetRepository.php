<?php

namespace Letgo\DomainInfraBundle\Repositories;

use Letgo\Domain\Entities\Tweet;
use Letgo\Domain\Repositories\TweetRepositoryInterface;
use Letgo\Domain\Services\TwitterClientInterface;
use Lsw\MemcacheBundle\Cache\MemcacheInterface;

/**
 * Class TweetRepository
 * @package Letgo\DomainInfraBundle\Repositories
 */
class TweetRepository implements TweetRepositoryInterface, RespositoryInterface
{
    /** @var  TwitterClientInterface $twitterApiClient */
    protected $twitterApiClient;
    /** @var  MemcacheInterface $cacheManager */
    protected $cacheManager;

    /**
     * TweetRepository constructor.
     * @param TwitterClientInterface $twitterApiClient
     * @param MemcacheInterface $cache
     */
    public function __construct(TwitterClientInterface $twitterApiClient,MemcacheInterface $cache)
    {
        $this->twitterApiClient = $twitterApiClient;
        $this->cacheManager= $cache;
    }

    /**
     * @inheritDoc
     */
    public function getCacheMgr()
    {
        return $this->cacheManager;
    }


    /**
     * @param int $tweetId
     * @return Tweet|null
     */
    public function find($tweetId)
    {
        // TODO: Implement find() method.
    }

    /**
     * @param string $username
     * @param int $count
     * @return Tweet[]|null
     */
    public function findLastTweets($username, $count)
    {
        $json = $this->twitterApiClient->getUserTimeline($username, $count);
        if (!empty($json)) {
            $tweetsStd = json_decode($json);
            $tweets = array();
            foreach ($tweetsStd as $tweetStd) {
                $tweet = new Tweet();
                $tweet->setId($tweetStd->id);
                $tweet->setTwitterUserId($tweetStd->user->id);
                $tweet->setJsonResponse(json_encode($tweetStd));
                $tweets[] = $tweet;
            }

            return $tweets;
        }

        return null;
    }

    /**
     * @param string $username
     * @param int $count
     * @return string
     */
    public function findLastTweetsJson($username, $count)
    {
        $cacheKey = __FUNCTION__.$username.$count;
        $respCache = $this->cacheManager->get($cacheKey);
        if ($respCache) {
            return $respCache;
        }

        $json = $this->twitterApiClient->getUserTimeline($username, $count);
        if (!empty($json)) {
            $this->cacheManager->set($cacheKey, $json, 0, 120);
            return $json;
        }

        return null;
    }

    /**
     * @param Tweet $tweet
     * @return int
     */
    public function postTweet(Tweet $tweet)
    {
        // TODO: Implement postTweet() method.
    }
}