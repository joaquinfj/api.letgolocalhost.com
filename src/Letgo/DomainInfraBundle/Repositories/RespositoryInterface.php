<?php

namespace Letgo\DomainInfraBundle\Repositories;

use Lsw\MemcacheBundle\Cache\MemcacheInterface;

/**
 * Interface RespositoryInterface
 * @package Letgo\DomainInfraBundle\Repositories
 */
interface RespositoryInterface
{
    /**
     * @return MemcacheInterface
     */
    public function getCacheMgr();
}