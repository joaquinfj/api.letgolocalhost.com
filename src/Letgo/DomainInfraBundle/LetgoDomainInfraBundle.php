<?php

namespace Letgo\DomainInfraBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class LetgoDomainInfraBundle
 * @package Letgo\DomainInfraBundle
 */
class LetgoDomainInfraBundle extends Bundle
{
}
