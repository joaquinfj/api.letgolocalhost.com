<?php

namespace Letgo\DomainInfraBundle\Services;

use Letgo\Domain\Services\TwitterClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TwitterSimulatorClient
 * @package Letgo\DomainInfraBundle\Services
 */
class TwitterSimulatorClient implements TwitterClientInterface
{
    /** @var ContainerInterface $container */
    protected $container;

    /**
     * TwitterClient constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $class
     * @return object
     */
    protected function get($class)
    {
        return $this->container->get($class);
    }


    /**
     * @see https://dev.twitter.com/rest/reference/get/statuses/user_timeline
     * @param string $screenName
     * @param int $count
     * @return string|null
     */
    public function getUserTimeline($screenName, $count)
    {
        // @TODO: simulate random tweets to check Cache system
    }

    /**
     * statuses/show.json
     * @inheritDoc
     */
    public function getTweetById($tweetId)
    {
        // @TODO: simulate the tweet.
    }

}