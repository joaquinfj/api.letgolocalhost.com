<?php

namespace Letgo\DomainInfraBundle\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Tests\Oauth1;

use Letgo\Domain\Services\json;
use Letgo\Domain\Services\TwitterClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TwitterClient
 * @package Letgo\DomainInfraBundle\Services
 */
class TwitterClient implements TwitterClientInterface
{
    /** @var ContainerInterface $container */
    protected $container;

    /**
     * TwitterClient constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $class
     * @return object
     */
    protected function get($class)
    {
        return $this->container->get($class);
    }


    /**
     * @see https://dev.twitter.com/rest/reference/get/statuses/user_timeline
     * @param string $screenName
     * @param int $count
     * @return string|null
     */
    public function getUserTimeline($screenName, $count)
    {
        /** @var Client $client */
        $client = $this->get('guzzle.twitter.client');
        $query = 'statuses/user_timeline.json'.'?screen_name='.$screenName.'&count='.$count;
        $response = $client->get($query);
        if ($response) {
            return json_decode($response->getBody());
        }

        return null;
    }

    /**
     * statuses/show.json
     * @inheritDoc
     */
    public function getTweetById($tweetId)
    {
        /** @var Client $client */
        $client = $this->get('guzzle.twitter.client');
        $query = 'statuses/show.json'.'?id='.$tweetId.'';
        $response = $client->get($query);
        return $response->getBody();
    }

}